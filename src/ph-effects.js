
(function(){

	var PHEffects = {};
	PHEffects.explosion = function(options)
	{
		var particles = [];

		var options 				= options 						|| {};
		options.speed 				= options.speed 				|| PHUtility.randNum(60,100);
		options.acceleration 		= options.acceleration 			|| PHUtility.randNum(-100,200);
		options.count 				= options.count 				|| 10;
		options.decay 				= options.decay 				|| 30;
		
		for (var angle=0; angle < 360; angle += Math.round(360/options.count))
		{
			var particle = new PHParticle({
				pos:options.pos,
				speed:options.speed,
				acceleration:options.acceleration,
				angle:angle,
				color: options.color,
				decay:options.decay
			});
			
			particles.push(particle);
		}
		
		return particles;
	};
	
	PHEffects.smokePuff = function(options)
	{
		var particles = [];

		var options 				= options 						|| {};
		options.count 				= options.count 				|| 10;
		options.decay 				= options.decay 				|| 20;
		options.life 				= options.life 					|| 10000;
		
		for (var i = 0;i < options.count;i++)
		{
			options.speed 				= PHUtility.randNum(-20,-30);
			options.acceleration 		= PHUtility.randNum(-1,-10);
			options.angle 				= PHUtility.randNum(80,110);
			options.size 				= PHUtility.randNum(1,10);
					
			var particle = new PHParticleImg(
			
				PHSimpleSprite.smokePuff({
					reverse:128,
					life:options.life,
					renderWidth:options.size,
					renderHeight:options.size,
					pos:options.pos,
					speed:options.speed,
					acceleration:options.acceleration,
					angle:options.angle,
					color: options.color,
					decay:options.decay})
				
			);
			
			particles.push(particle);
		}
		
		return particles;
	};
	
	PHEffects.sparkle = function(options)
	{
		var particles = [];

		var options 				= options 							|| {};
		options.count 				= options.count 					|| 20;
		options.gravity 			= options.gravity 					|| 1;
		options.angle 				= options.angle 					|| PHUtility.randNum(0,360);
		options.color 				= options.color 					|| [PHUtility.randNum(0,360),100,PHUtility.randNum(50,80),1.0];
		options.decay 				= options.decay 					|| 1.4;
		options.radius 				= options.radius 					|| 2;
		options.coordinateCount		= options.coordinateCount 			|| 10;
		
		for (var i = 0;i < options.count;i++)
		{
			options.speed 			= PHUtility.randNum(50,100);
			options.acceleration 	= PHUtility.randNum(10,40);
			options.angle 			= PHUtility.randNum(0,360);
		
			var particle = new PHSpark(options);
			particles.push(particle);
		}
		
		return particles;
	};
	
	PHInject(PHEffects,"PHEffects");
	
})();

