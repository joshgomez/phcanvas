
(function(){

	function PHSnow(options)
	{
		var options = options || {};
		
		options.radius 		= options.radius 	|| PHUtility.randNum(1,5);
		options.density 	= options.density 	|| PHUtility.randNum(1,25);
		options.color 		= options.color 	|| [255,255,255,1];
		options.speed 		= options.speed 	|| 5;
		
		PHArc.call(this,options);
		PHMovement.call(this,options);

		this.density = options.density;
	}

	PHSnow.prototype = Object.create(PHArc.prototype);
	PHSnow.prototype.constructor = PHSnow;

	PHSnow.prototype.animate = function(canvas,index)
	{
		if(index%3 > 0) this.angle = PHUtility.randNum(100,110);
		else this.angle = 85;
		
		this.setAcceleration(this.radius+PHUtility.randNum(1,25));
		var acceleration = this.getVelocity(canvas);
		this.setSpeed(acceleration);

		var direction = PHUtility.degToCoord(this.angle);
		var newPos = this.getMovePos(direction,canvas);
		this.setPos(newPos[0],newPos[1]);
		
		if(this.getY() + this.radius > canvas.getHeight())
		{
			canvas.remove(this.id,index);
		}
	};

	PHInject(PHSnow,"PHSnow");
	
})();

