
(function(){
	
	function PHPlayer(options)
	{
		var options = options || {};
		options.speed = 15;
		
		PHEntity.call(this,options);
		
		this.isMovingLeft 		= false;
		this.isMoving 			= false;
		this.isIdle 			= false;
		this.isJumping 			= false;
		this.isFalling 			= true;
		
		this.bJump 				= false;
		this.bMoveLeft 			= false;
		this.bMoveRight 		= false;
		
		this.sJump 	= 500;
		this.sFall 	= 10;
		this.sMove 	= 20;
		
		this.moveAngle = 0;
		
		PHSprite.simpleCube.call(this,options);
		this.updateSAT(options.pos);

		return this;
	}

	PHPlayer.prototype = Object.create(PHEntity.prototype);
	PHPlayer.prototype.constructor = PHPlayer;
	
	PHPlayer.prototype.moveLeft = function()
	{
		if(!this.isMoving)
		{
			this.moveAngle = 180;
			this.isMovingLeft = true;
			this.isMoving = true;
			this.setSpeed([100,this.getSpeed(1)]);
		}
	};
	
	PHPlayer.prototype.moveRight = function()
	{
		if(!this.isMoving)
		{
			this.moveAngle = 0;
			this.isMovingLeft = false;
			this.isMoving = true;
			this.setSpeed([100,this.getSpeed(1)]);
		}
	};
	
	PHPlayer.prototype.jump = function()
	{
		if(!this.isJumping && !this.isFalling)
		{
			this.isJumping = true;
			this.setSpeed([this.getSpeed(0),250]);
		}
	};
	
	PHPlayer.prototype.checkMove = function(canvas)
	{
		if(this.isMoving)
		{
			this.isIdle = false;
			if(!this.isJumping && !this.isFalling)
			{
				this.setAcceleration([this.sMove,this.getAcceleration(1)]);
				var velocity = this.getVelocity(canvas,150);
				this.setSpeed([velocity[0],this.getSpeed(1)]);
			}
			
			var direction = PHUtility.degToCoord(this.moveAngle);
			var newPos = this.getMovePos(direction,canvas);
			
			this.setPos(newPos[0],this.getY());
			this.model.setPos(newPos[0],this.getY());
			this.updateSAT(this.model.intPos);
			
			if(SAT.testPolygonPolygon(this.SAT, canvas.SAT.left))
			{
				this.isMoving = false;
				this.setPos(0,this.getY());
				this.model.setPos(0,this.getY());
			}
			
			if(SAT.testPolygonPolygon(this.SAT, canvas.SAT.right))
			{
				this.isMoving = false;
				this.setPos(canvas.getWidth() - 32,this.getY());
				this.model.setPos(canvas.getWidth() - 32,this.model.getY());
			}
		}
	};
	
	PHPlayer.prototype.checkJump = function(canvas)
	{
		if(this.isJumping || this.isFalling)
		{
			this.isIdle = false;
			this.isMoving = false;
			this.angle = 270;
			this.setAcceleration([this.getAcceleration(0),-(this.sJump)]);
			
			var velocity = this.getVelocity(canvas);
			
			if(velocity[1] <= 0)
			{
				this.isJumping = false;
				this.isFalling = true;
			}
			
			this.setSpeed([this.getSpeed(0),velocity[1]]);
			
			var direction = PHUtility.degToCoord(this.angle);
			var newPos = this.getMovePos(direction,canvas);
			
			this.setPos(this.getX(),newPos[1]);
			this.model.setPos(this.getX(),newPos[1]);
			this.updateSAT(this.model.intPos);
			
			if(SAT.testPolygonPolygon(this.SAT, canvas.SAT.bottom))
			{
				this.isFalling = false;
				this.setPos(this.getX(),canvas.getHeight() - 32);
				this.model.setPos(this.model.getX(),canvas.getHeight() - 32);
			}
		}
	};
	
	PHPlayer.prototype.serverData = function(data)
	{
		var data 			= data || {};
		data.isMovingLeft 	= this.isMovingLeft;
		data.isMoving 		= this.isMoving;
		data.isIdle 		= this.isIdle;
		data.isJumping 		= this.isJumping;
		data.isFalling 		= this.isFalling;
		
		data.jump 			= this.bJump;
		data.moveLeft 		= this.bMoveLeft;
		data.moveRight 		= this.bMoveRight;
		
		data.pos 			= this.pos;
		
		return data;
	};
	
	PHPlayer.prototype.resetMovements = function()
	{
		this.bJump 				= false;
		this.bMoveLeft 			= false;
		this.bMoveRight 		= false;
	};
	
	PHPlayer.prototype.update = function(canvas)
	{
		if(this.bJump) this.jump();
		
		if(this.bMoveLeft) this.moveLeft();
		else if(this.bMoveRight) this.moveRight();
		else this.isMoving = false;
		
		this.checkMove(canvas);
		this.checkJump(canvas);
		
		if(!this.isMoving && !this.isJumping && !this.isFalling) this.isIdle = true;
		else this.isIdle = false;
	};

	PHInject(PHPlayer,"PHPlayer");
	
})();

