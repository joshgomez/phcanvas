	
(function(){

	function PHResourceHandler()
	{
		this.resourceCache = {};
		this.loading = [];
		this.readyCallbacks = [];
		
		this.audioExts = [];
		this._init();
	}

	PHResourceHandler.prototype = {
	
		_init:function()
		{
			var audio = new Audio();
			if(audio.canPlayType('audio/ogg;')) 	this.audioExts.push('ogg');
			if(audio.canPlayType('audio/mpeg;')) 	this.audioExts.push('mp3');
			if(audio.canPlayType('audio/wav;')) 	this.audioExts.push('wav');
		},

		load:function(urlArray){
		
			var self = this;
			if(urlArray instanceof Array){
			
				urlArray.forEach(function(url)
				{
					self._load(url);
				});
			}
			else
			{
				this._load(urlArray);
			}
		},
		
		setResource:function(file,index)
		{
			this.resourceCache[index] = file;
			if(this.isReady()){
			
				this.readyCallbacks.forEach(function(func) { func(); });
			}
		},
		
		_load:function(url){
		
			var filename = url.split('.').shift().toLowerCase();
			
			if(this.resourceCache[filename]){
			
				return this.resourceCache[filename];
			}
			else
			{
				var self = this;

				function loopAudio(name,index)
				{
					var filepath = name + '.' + self.audioExts[index];
					
					PHUtility.testAudio(filepath,function(snd,result){
					
						if(result == 'success')
						{
							self.setResource(snd,name);
						}
						else if(index < self.audioExts.length)
						{
							index++;
							loopAudio(name,index);
						}
					});
				}
				
				PHUtility.testImage(url,function(img,result){
				
					if(result == 'success')
					{
						self.setResource(img,filename);
					}
					else
					{
						loopAudio(filename,0);
					}
				});
				
				this.resourceCache[filename] = false;
			}
		},
		
		get:function(index){
		
			return this.resourceCache[index];
		},
		
		isReady:function(){
		
			var ready = true;
			for(var k in this.resourceCache)
			{
				if(this.resourceCache.hasOwnProperty(k) && !this.resourceCache[k])
					ready = false;
			}
			
			return ready;
		},
		
		onReady:function(func)
		{
			this.readyCallbacks.push(func);
		}
	};
	
	PHInject(PHResourceHandler,"PHResourceHandler");
	
	PHInject(new PHResourceHandler(),"PHPreResource");
	PHInject(new PHResourceHandler(),"PHResource");

})();