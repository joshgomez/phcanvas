
(function(){
	
	function PHEntity(options)
	{
		var options = options || {};
		
		PHPos.call(this,options);
		PHMovement.call(this,options);
		
		this.id = options.id;
		this.SAT;
		
		return this;
	}

	PHEntity.prototype = {

		getID:function(){return this.id;},
		
		updateSAT:function(pos)
		{
			var vectors = [];
			for(var i = 0;i < this.points.length;i++)
			{
				vectors.push(new SAT.Vector(this.points[i][0],this.points[i][1]));
			}

			this.SAT = new SAT.Polygon(new SAT.Vector(pos[0],pos[1]),vectors);
		}
	};

	PHInject(PHEntity,"PHEntity");
	
})();

