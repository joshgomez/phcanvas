
(function(){

	window = window || {};
	
	window.AudioContext = 
		window.AudioContext 		|| 
		window.webkitAudioContext 	|| 
		window.mozAudioContext 		|| 
		window.msAudioContext 		||
		window.oAudioContext;
	
	window.cancelAnimationFrame = 
		window.cancelAnimationFrame 		|| 
		window.webkitCancelAnimationFrame 	|| 
		window.mozCancelAnimationFrame 		|| 
		window.msCancelAnimationFrame 		||
		window.oRequestAnimationFrame;
	
	window.requestAnimationFrame = 
		window.requestAnimationFrame 		|| 
		window.webkitRequestAnimationFrame 	|| 
		window.mozRequestAnimationFrame 	||
		window.msRequestAnimationFrame 		||
		window.oRequestAnimationFrame;

	if(!window.requestAnimationFrame)
	{
		var lastTime = 0;
		window.requestAnimationFrame = function(callback,element)
		{
			var currTime = Date.now()
			var timeToCall = Math.max( 0, 1000/60 - ( currTime - lastTime ) );
			
			var id = window.setTimeout(function(){ callback( currTime + timeToCall ); }, timeToCall );
			
			lastTime = currTime + timeToCall;
			return id;
		};
	}
	
	if (!window.cancelAnimationFrame)
	{
		window.cancelAnimationFrame = function ( id ) { clearTimeout( id ); };
	}
	
	if(typeof module !== 'undefined' && module.exports)
		exports.window = window;
		
})();
	
(function(){

	function PHInject(obj,name)
	{
		if(typeof module !== 'undefined' && module.exports)
			exports[name] = obj;
		else window[name] = obj;	
	}
	
	if(typeof module !== 'undefined' && module.exports)
		exports.PHInject = PHInject;
	else window.PHInject = PHInject;
	
})();