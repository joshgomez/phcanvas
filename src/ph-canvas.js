

(function(){

	function PHCanvas(el,options)
	{
		var options 				= options 						|| {};
		options.showFPS 			= options.showFPS 				|| false;
		options.debugKey 			= options.debugKey 				|| false;

		this.el 		= document.getElementById(el);
		this.context 	= this.el.getContext('2d');
		this.loop 		= null;
		
		this.buffer = {};
		
		this._showFPS 		= options.showFPS;
		this._debugKeyDown 	= options.debugKey;
		
		this._keymap = [];
		this._keyDown = false;
		
		this._mousePos = [0,0];
		this._mouseDown = false;
		
		this._active 		= 1;
		this._now 			= 0;
		this._lastTime 		= 0;
		this._delta 		= 0;
		this._fps 			= 0;
		
		this._width 		= this.el.width;
		this._height 		= this.el.height;

		this._init();
		this._thread();
		
		this.SAT = {};
	}
		
	PHCanvas.prototype = {
		
		_init:function()
		{
			if(this._showFPS)
				this.set('showfps',new PHText().setPos(4,12).setStroke(2),9999);
				
			var self = this;
			var keypress = function(e)
			{
				var e = e || event;
				self._keymap[e.keyCode] = (e.type == 'keydown' || e.type == 'keypress');
				self._keyDown = (e.type == 'keydown' || e.type == 'keypress');
				
				if(self._debugKeyDown && e.type == 'keydown')
				{
					console.log("Keydown: " + self.getKeyChar(e.keyCode) + "(" + e.keyCode + ")");
				}
			}
		
			window.addEventListener("keypress",keypress);
			window.addEventListener("keydown",keypress);
			window.addEventListener("keyup",keypress);
			
			window.addEventListener("blur",function(e)
			{
				self._keymap = [];
				self._active = 0;
			
			});
			
			window.addEventListener("focus",function(e)
			{
				self._keymap = [];
				self._active = 1;
			
			});
			
			this.el.addEventListener("mousemove", function(e)
			{
				var x = e.pageX - e.target.offsetLeft;
				var y = e.pageY - e.target.offsetTop;
				self._mousePos = [x,y];
			});
			
			this.el.addEventListener("mousedown", function(e)
			{
				self._mouseDown = true;
			});
			
			this.el.addEventListener("mouseup", function(e)
			{
				self._mouseDown = false;
			});
		},
		
		getSizeRatio:function()
		{
			var ratio = Math.min((this._height / 320), (this._width / 480));
			
			if(ratio >= 1)
				return 1;

			return ratio;
		},
		
		getKey:function(key)
		{
			if(this._keymap[key])
				return true;
		
			return false;
		},
		
		getKeyChar:function(key)
		{
			var str = "";
			try
			{
				str = String.fromCharCode(key);
			}
			catch(e){}
			
			return str;
		},
		
		getKeyDown:function(){return this._keyDown;},
		getMouseDown:function(){return this._mouseDown;},
		getMousePos:function(){return this._mousePos;},
		getActive:function(){return this._active;},
		getWidth:function(){return this._width;},
		getHeight:function(){return this._height;},
		getSize:function(){return [this._width,this._height];},
		
		getNow:function(){return this._now;},
		getDelta:function(){return this._delta;},
		getFrameTime:function(){return this._delta;},
		getFPS:function(){return this._fps.toFixed(1);},
		getFNow:function(){return this.getNow() + this.getDelta()*1000;},
		
		setSize:function(width,height)
		{
			this.el.width = width;
			this.el.height = height;
		
			this._width 	= width;
			this._height 	= height;
			
			this.SAT.top 			= new SAT.Box(new SAT.Vector(0,0), width, 1).toPolygon();
			this.SAT.left 			= new SAT.Box(new SAT.Vector(0,0), 1, height).toPolygon();
			this.SAT.bottom 		= new SAT.Box(new SAT.Vector(0,height), width, 1).toPolygon();
			this.SAT.right 			= new SAT.Box(new SAT.Vector(width,0), 1, height).toPolygon();
		},
		
		set:function(key,obj,level)
		{
			var level = level || 0;
			
			if(Array.isArray(obj))
			{
				for(var i = 0;i < obj.length;i++)
				{
					obj[i].setId(key);
				}
			}
			else
			{
				obj.setId(key);
			}
			
			this.buffer[key] = {obj:obj,layer:level};
		},
		
		add:function(key,newObj)
		{
			if(this.buffer[key] && Array.isArray(this.buffer[key].obj))
			{
				newObj.setId(key);
				this.buffer[key].obj.push(newObj);
				return true;
			}
			
			return false;
		},
		
		setLayer:function(key,level)
		{
			if(this.buffer[key])
			{
				this.buffer[key].layer = level;
				return level;
			}
				
			return false;
		},
		
		get:function(key,index)
		{
			if(this.buffer[key])
			{
				if(typeof index != "undefined")
				{
					if(this.buffer[key].obj[index])
					{
						return this.buffer[key].obj[index];
					}
					
					return false;
				}
				
				return this.buffer[key].obj;
			}
				
			return false;
		},
		
		remove:function(key,index,recursive)
		{
			if(this.buffer[key])
			{
				if(typeof index != "undefined")
				{
					if(this.buffer[key].obj[index])
					{
						delete this.buffer[key].obj[index];
						this.buffer[key].obj.splice(index,1);
						
						if(this.buffer[key].obj.length == 0 && recursive)
						{
							delete this.buffer[key];
						}
						
						return true;
					}
				}
				else
				{
					delete this.buffer[key];
					return true;
				}
			}
			
			return false;
		},
		
		_clear:function()
		{
			this.context.clearRect(0, 0, this._width,this._height);
		},
		
		_thread:function()
		{
			this._now = Date.now();
			
			if(this._lastTime != 0 && this._active == 1)
			{
				this._delta = (this._now - this._lastTime) / 1000.0;
				var timeFrame = 1000/(this._now - this._lastTime);
				this._fps += (timeFrame - this._fps)/60;
				
				this._clear();
				
				if(this._showFPS)
				{
					var showfps = this.get('showfps');
					if(showfps)
					{
						showfps.setMsg('FPS: ' + this.getFPS() + ' Delta: ' + this.getDelta());
					}
				}
				
				var buffer = [];
				for (var key in this.buffer)
					buffer.push(this.buffer[key]);
				
				buffer.sort(function(a, b){
					return a.layer - b.layer;
				});
				
				for(var i = 0; i < buffer.length;i++)
				{
					if(Array.isArray(buffer[i].obj))
					{
						for(var j = 0; j < buffer[i].obj.length;j++)
						{
							buffer[i].obj[j].setIndex(j);
							if(buffer[i].obj[j].animate)
								buffer[i].obj[j].animate(this,j);
							
							if(typeof buffer[i].obj[j] == 'undefined') continue;
							if(buffer[i].obj[j].update)
								buffer[i].obj[j].update(this,j);
								
							if(typeof buffer[i].obj[j] == 'undefined') continue;
							if(this.updateAll)
								this.updateAll(buffer[i].obj[j],j);
						
							if(typeof buffer[i].obj[j] == 'undefined') continue;
							if(buffer[i].obj[j].draw)
								buffer[i].obj[j].draw(this,j);
						}
						
						continue;
					}
					
					if(buffer[i].obj.animate)
						buffer[i].obj.animate(this);
						
					if(typeof buffer[i].obj == 'undefined') continue;
					if(buffer[i].obj.update)
						buffer[i].obj.update(this);
						
					if(typeof buffer[i].obj == 'undefined') continue;
					if(this.updateAll)
						this.updateAll(buffer[i].obj);
						
					if(typeof buffer[i].obj == 'undefined') continue;
					if(buffer[i].obj.draw)
						buffer[i].obj.draw(this);
				}
			}
			
			this._lastTime = this._now;
			this.loop = window.requestAnimationFrame(this._thread.bind(this));
		}
	};
	
	PHInject(PHCanvas,"PHCanvas");

})();