
(function(){

	var PHUtility = {};
	PHUtility.degToRad = function(degrees)
	{
		return degrees * (Math.PI/180);
	};
	
	PHUtility.radToDeg = function(radians)
	{
		return radians * (180/Math.PI);
	};
	
	PHUtility.degToCoord = function(deg)
	{
		var x = Math.cos(PHUtility.degToRad(deg));
		var y = Math.sin(PHUtility.degToRad(deg));
		
		return [x,y];
	};
	
	PHUtility.rand = function(min,max)
	{
		return Math.random() * ( max - min ) + min;
	};
	
	PHUtility.randNum = function(min,max)
	{
		var random = Math.random() * ( max - min + 1) + min;
		return Math.floor(random);
	};
	
	PHUtility.calDist = function(x1,y1,x2,y2)
	{
		return Math.sqrt( Math.pow( (x2-x1), 2 ) + Math.pow((y2-y1), 2 ) );
	};
	
	PHUtility.hexToRGB = function(hex)
	{
		var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
		hex = hex.replace(shorthandRegex, function(m, r, g, b) {
			return r + r + g + g + b + b;
		});

		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		return result ? {
			r: parseInt(result[1], 16),
			g: parseInt(result[2], 16),
			b: parseInt(result[3], 16)
		} : null;
	};
	
	PHUtility.hslToRGB = function(h,s,l)
	{
		function HueToRGB(m1,m2,hue)
		{
			var v;
			if (hue < 0)
				hue += 1;
			else if (hue > 1)
				hue -= 1;

			if (6 * hue < 1)
				v = m1 + (m2 - m1) * hue * 6;
			else if (2 * hue < 1)
				v = m2;
			else if (3 * hue < 2)
				v = m1 + (m2 - m1) * (2/3 - hue) * 6;
			else
				v = m1;

			return 255 * v;
		}
		
		var m1, m2, hue;
		var r, g, b;
		
		s /=100;
		l /= 100;
		if (s == 0) r = g = b = (l * 255);
		else
		{
			if (l <= 0.5) m2 = l * (s + 1);
			else m2 = l + s - l * s;
			
			m1 = l * 2 - m2;
			hue = h / 360;
			r = HueToRGB(m1, m2, hue + 1/3);
			g = HueToRGB(m1, m2, hue);
			b = HueToRGB(m1, m2, hue - 1/3);
		}
		
		return {r:Math.round(r),g:Math.round(g),b:Math.round(b)};
	};
	
	PHUtility.testImage = function(url, callback, timeout)
	{
		var timeout = timeout || 5000;
		var timedOut = false, timer;
		
		var img = new Image();
		img.onerror = img.onabort = function(e)
		{
			if (!timedOut)
			{
				clearTimeout(timer);
				callback(img, "error");
			}
		};
		
		img.onload = function(e)
		{
			if (!timedOut)
			{
				clearTimeout(timer);
				callback(img, "success");
			}
		};
		
		img.src = url;
		timer = setTimeout(function(e)
		{
			timedOut = true;
			callback(img, "timeout");
		},timeout); 
	};
	
	PHUtility.testAudio = function(url, callback, timeout)
	{
		var timeout = timeout || 60000;
		var timedOut = false, timer;
		
		var snd = new Audio();
		snd.onerror = snd.onabort = function(e)
		{
			if (!timedOut)
			{
				clearTimeout(timer);
				callback(snd, "error");
				console.log(e);
			}
		};
		
		snd.oncanplaythrough = function(e)
		{
			if (!timedOut)
			{
				clearTimeout(timer);
				callback(snd, "success");
			}
		};
		
		snd.src = url;
		timer = setTimeout(function(e)
		{
			timedOut = true;
			callback(snd, "timeout");
		},timeout); 
	};
	
	PHInject(PHUtility,"PHUtility");
	
})();

