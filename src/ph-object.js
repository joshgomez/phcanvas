
(function(){

	function PHPos(options)
	{
		options.pos = options.pos || [0,0];
		
		this.intPos = [];
		
		this.setPos = function(x,y){

			var pos = x;
			if(!Array.isArray(x))
			{
				pos = [x,y];
			}
			
			this.pos = pos; 
			this.intPos[0] = Math.ceil(this.pos[0]); 
			this.intPos[1] = Math.ceil(this.pos[1]);
			
			return this;
		};
		
		this.setPos(options.pos[0],options.pos[1]);
		
		this.getPos		= function(){return this.pos;};
		this.getX 		= function(){return this.pos[0];};
		this.getY 		= function(){return this.pos[1];};
	}



	function PHMovement(options)
	{
			options.speed 				= options.speed 			|| [100,100];
			options.acceleration 		= options.acceleration 		|| [0,0];
			options.gravity 			= options.gravity 			|| 0;
			options.angle 				= options.angle 			|| 0;
			
			this.angle 		= options.angle;
			
			this.gravity 	= 0;
			this.gravityFactor 	= options.gravity;
			
			this.setGravity = function(amount){
				
				this.gravityFactor = amount;
				return this;
			};
			
			this.setSpeed = function(amount){
			
				if(Array.isArray(amount))
				{
					this.speed = [amount[0],amount[1]];
					return this;
				}
				
				this.speed = [amount,amount];
			};
			
			this.setAcceleration = function(amount)
			{
				if(Array.isArray(amount))
				{
					this.acceleration = [amount[0],amount[1]];
					return this;
				}
				
				this.acceleration = [amount,amount];
				return this;
			};
			
			this.getSpeed = function(coord)
			{
				switch(coord)
				{
					case "y":
					case "Y":
					case "1":
					case 1:
						return this.speed[1];
						break;
					case "x":
					case "X":
					case "0":
					case 0:
						return this.speed[0];
				}
				
				return this.speed;
			};
			
			this.getAcceleration = function(coord)
			{
				switch(coord)
				{
					case "y":
					case "Y":
					case "1":
					case 1:
						return this.acceleration[1];
						break;
					case "x":
					case "X":
					case "0":
					case 0:
						return this.acceleration[0];
						break;
				}
				
				return this.acceleration;
			};
			
			this.getMovePos = function(direction,canvas)
			{
				this.gravity += this.gravityFactor*canvas.getDelta();
			
				var pos = [];
				pos[0] = this.pos[0] + (direction[0] * this.speed[0])*canvas.getDelta();
				pos[1] = this.pos[1] + (direction[1] * this.speed[1])*canvas.getDelta();
				
				pos[1] += this.gravity;
				
				return pos;
			};
			
			this.getVelocity = function(canvas,max)
			{
				var speed = [];
				speed[0] = this.speed[0] + this.acceleration[0]*canvas.getDelta();
				speed[1] = this.speed[1] + this.acceleration[1]*canvas.getDelta();
				
				if(max)
				{
					var max = max;
					if(!Array.isArray(max))
					{
						max = [max,max];
					}
					
					if(speed[0] > max[0]) speed[0] = this.speed[0];
					if(speed[1] > max[1]) speed[1] = this.speed[1];
				}
				
				return speed;
			};
			
			this.setAcceleration(options.acceleration);
			this.setSpeed(options.speed);
	}

	function PHObject(options)
	{
		var options 	= options 			|| {};
		options.color 	= options.color 	|| [0,0,0,1.0];
		options.rotate 	= options.rotate 	|| 0;
		options.parent 	= options.parent 	|| null;

		this.id;
		this.index;
		this.parent = options.parent;
		
		PHPos.call(this,options);
		
		this.color = options.color;
		this.rotate = options.rotate; 
		
		this._fadeIn = 0;
		this._fadeOut = 0;
		
		this._fadeDate = 0;
		this._deleteAfter = 0;
		
		this.created = Date.now();
		
		return this;
	}

	PHObject.prototype = {

		setId:function(id){this.id = id; return this;},
		setIndex:function(index){this.index = index; return this;},
		setParent:function(parent){this.parent = parent; return this;},
		
		setColor:function(red,green,blue,alpha)
		{
			this.color = [red,green,blue,alpha];
			if(Array.isArray(red)) this.color = red;
			
			return this;
		},
		
		setRed:function(value){this.color[0] = value; return this;},
		setGreen:function(value){this.color[1] = value; return this;},
		setBlue:function(value){this.color[2] = value; return this;},
		setAlpha:function(value){this.color[3] = value; return this;},
		setRotate:function(value){this.rotate = value; return this;},
		
		setHue:function(value){return setRed(value);},
		setSaturation:function(value){return setGreen(value);},
		setBrightness:function(value){return setBlue(value);},
		
		deleteAfter:function(time)
		{
			var time = time || 1;
			this._deleteAfter = time;
			return this;
		},
		
		getID:function(){return this.id;},
		getIndex:function(){return this.index;},
		getParent:function(){return this.parent;},
		
		getAlpha:function(){return this.color[3];},
		
		fadeOut:function(time,deleteAfter)
		{
			if(this._fadeIn > 0) return this;
		
			if(this._fadeOut == 0)
			{
				var time = time || 1000;
				
				this._fadeDate = Date.now();
				this._fadeOut = time;
				
				if(deleteAfter)
					this.deleteAfter(1);
			}
			
			return this;
		},
		
		fadeIn:function(time,deleteAfter)
		{
			if(this._fadeOut > 0) return this;
		
			if(this._fadeIn == 0)
			{
				var time = time || 1000;
				
				this._fadeDate = Date.now();
				this._fadeIn = time;
				
				if(deleteAfter > 0)
					this.deleteAfter(deleteAfter);
			}
			
			return this;
		},
		
		__fadeOut:function(canvas)
		{
			if(this._fadeOut > 0)
			{
				range = (this._fadeDate + this._fadeOut) - canvas.getFNow();
				if(range <= 0)
				{
					range = 0;
					
					if(this._deleteAfter > 0) canvas.remove(this.id);
					else this._fadeOut = 0;
				}
				
				var alpha = range/this._fadeOut;
				if(alpha >= this.getAlpha()) return;
				
				this.setAlpha(alpha);
			}
		},
		
		__fadeIn:function(canvas)
		{
			if(this._fadeIn > 0)
			{
				var range = canvas.getFNow() - this._fadeDate;
				if(range >= this._fadeIn)
				{
					range = this._fadeIn;
					
					if(this._deleteAfter > 0)
					{
						var self = this;
						setTimeout(function(){canvas.remove(self.id)},this._deleteAfter);
					}
					else this._fadeIn = 0;
				}
				
				var alpha = range/this._fadeIn;
				if(alpha <= this.getAlpha()) return;
				
				this.setAlpha(alpha);
			}
		}
	};
	
	PHInject(PHPos,"PHPos");
	PHInject(PHMovement,"PHMovement");
	PHInject(PHObject,"PHObject");
	
})();

