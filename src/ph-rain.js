
(function(){

	function PHRain(options)
	{
			var options 			= options 					|| {};
			options.startPos 		= options.startPos 			|| [0,0];
			options.endPos 			= options.endPos 			|| [0,0];
			options.speed 			= options.speed 			|| 600;
			options.acceleration 	= options.acceleration 		|| 160;
			options.color 			= options.color 			|| [235,100,PHUtility.randNum( 50, 70 ),0.5];
			
			PHObject.call(this,options);
			PHMovement.call(this,options);
			
			this.startPos 	= options.startPos;
			this.endPos 	= options.endPos;
			this.setPos(this.startPos[0],this.startPos[1]);
			
			this.distanceTraveled = 0;
			this.targetRadius=1;
			this.distanceToTarget = 0;
			this.coords = [];
			this.coordinateCount = 2;
			
			this.init();
			
			return this;
	}

	PHRain.prototype = Object.create(PHObject.prototype);
	PHRain.prototype.constructor = PHRain;

	PHRain.prototype.init = function()
	{
		this.angle 				= PHUtility.radToDeg(Math.atan2(this.endPos[1] - this.startPos[1],  this.endPos[0] - this.startPos[0]));
		this.distanceToTarget 	= PHUtility.calDist(this.startPos[0],this.startPos[1], this.endPos[0], this.endPos[1]);

		while(this.coordinateCount--)
		{
			this.coords.push([this.startPos[0],this.startPos[1]]);
		}
	};

	PHRain.prototype.draw=function(canvas)
	{
		canvas.context.save();

		canvas.context.globalCompositeOperation = "lighter";
		
		canvas.context.beginPath();

		canvas.context.moveTo(this.coords[ this.coords.length - 1][0],this.coords[ this.coords.length - 1][1]);
		canvas.context.lineTo( this.intPos[0], this.intPos[1] );
		canvas.context.lineWidth = 1;
		canvas.context.strokeStyle = 'hsla(' + this.color[0] + ', ' + this.color[1] + '%, ' + this.color[2] + '%, ' + this.color[3] + ')';
		canvas.context.stroke();
		
		canvas.context.closePath();
		
		canvas.context.restore();
	};

	PHRain.prototype.animate=function(canvas,index)
	{
		this.coords.pop();
		this.coords.unshift([this.intPos[0],this.intPos[1]]);
		
		var acceleration = this.getVelocity(canvas);
		this.setSpeed(acceleration);
		
		var direction = PHUtility.degToCoord(this.angle);
		var newPos = this.getMovePos(direction,canvas);

		this.distanceTraveled = PHUtility.calDist(this.startPos[0],this.startPos[1], newPos[0], newPos[1]);

		if(this.distanceTraveled >= this.distanceToTarget + newPos[1])
		{
			canvas.remove(this.id,index);
			return;
		}

		this.setPos(newPos[0],newPos[1]);
	}; 

	PHInject(PHRain,"PHRain");
	
})();

