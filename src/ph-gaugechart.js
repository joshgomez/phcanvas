
(function(){
	
	function PHGaugeChart(options)
	{
		var options = options || {};
		
		options.degrees 		= options.degrees 	|| 90;
		options.radius 			= options.radius 	|| 24;
		options.bgcolor 		= options.bgcolor 	|| [100,100,100,0.7];
		options.color 			= options.color 	|| [255,0,0,0.9];
		options.size 			= options.size 		|| 14;
		options.width 			= options.width 	|| 8;

		PHArc.call(this,options);
		
		this.bgcolor 	= options.bgcolor;
		this.degrees 	= options.degrees;
		this.width 		= options.width
		this.text 		= new PHText(options).setAlign("center").setBaseline("middle");
		
		this.setPosTemp = this.setPos;
		this.setPos = function(x,y)
		{
			this.setPosTemp(x,y);
			this.text.setPos(x,y);
			return this;
		};
	}

	PHGaugeChart.prototype = Object.create(PHArc.prototype);
	PHGaugeChart.prototype.constructor = PHGaugeChart;

	PHGaugeChart.prototype.setFactor = function(factor){this.degrees = 360*factor; return this;};
	PHGaugeChart.prototype.getSize = function(){return this.width/2 + this.radius;};

	PHGaugeChart.prototype.draw = function(canvas,index)
	{
		canvas.context.save();
		
		canvas.context.beginPath();
		canvas.context.strokeStyle = 'rgba(' + this.bgcolor.join() + ')';
		canvas.context.lineWidth = this.width;
		canvas.context.arc(this.intPos[0], this.intPos[1], this.radius, 0, Math.PI*2, false);
		canvas.context.stroke();
		canvas.context.closePath();

		var radians = Math.ceil(PHUtility.degToRad(this.degrees));

		canvas.context.beginPath();
		canvas.context.strokeStyle = 'rgba(' + this.color.join() + ')';
		canvas.context.lineWidth = this.width;
		canvas.context.arc(this.intPos[0], this.intPos[1], this.radius, 0 - PHUtility.degToRad(90), radians - PHUtility.degToRad(90), false); 
		canvas.context.stroke();
		canvas.context.closePath();
		
		canvas.context.restore();
		
		if(this.text)
		{
			this.text.draw(canvas,index);
		}
	};

	PHGaugeChart.prototype.animate = function(canvas,index)
	{
		var percent = Math.floor(this.degrees/360*100) + "%";
		if(this.text)
		{
			this.text.setMsg(percent);
		}
	};
	
	PHInject(PHGaugeChart,"PHGaugeChart");
	
})();

