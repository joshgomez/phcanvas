
(function(){

	var PHSimpleSprite = {};
	
	PHSimpleSprite.explosionTypeA = function(options)
	{
		var options 		= options || {};
		
		options.img 		= PHResource.get('pub/img/sprites/exp_type_A');
		options.frames 		= 33;
		options.width 		= 128;
		options.height 		= 128;

		return options;
	};
	
	PHSimpleSprite.smokePuff = function(options)
	{
		var options = options || {};
		
		options.img 		= PHResource.get('pub/img/smoke10');
		options.frames 		= 1;
		options.width 		= 256;
		options.height 		= 256;

		return options;
	};
	
	function PHSprite()
	{
		this.points = [];
		this.model 	= null;
	}

	PHSprite.simpleCube = function(options) 
	{
		this.points  = [
		
			[0,0],
			[32,0],
			[32,32],
			[0,32]
		
		];
		
		this.model = new PHRect(
		{
			pos:options.pos,
			width:32,
			height:32,
			color:[255,255,0,1],
			parent:this
		});
	};
	
	PHInject(PHSimpleSprite,"PHSimpleSprite");
	PHInject(PHSprite,"PHSprite");
	
})();

