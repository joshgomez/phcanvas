
(function(){

	function PHAudio(el,options)
	{
		this.el 			= el;
	
		var options = options || {};
		
		options.volume 	= (!options.volume) 	|| 1.0;
		options.loop 	= options.loop 			|| false;
		
		this.volume 		= options.volume;
		this.loop 			= options.loop;
		
		this.audioContext 	= null;
		this.file 			= null;
		this.buffer 		= null;
		this.gainNode 		= null;
		this.splitter 		= null;
		this.source 		= null;
		
		this.processor 		= [];
		this.analyser 		= [];
		
		this.frequencyBinCount 	= [];
		
		this.frequencyArray 	= [];
		this.timeDomainArray 	= [];
		
		this.playing 		= false;
		this.startOffset 	= 0;
		this.startTime 		= 0;

		this._init();
	}

	PHAudio.prototype = {
		_init:function()
		{
			try
			{
				this.audioContext = new AudioContext();
				this.file = this.el.src;
				this._load();
			}
			catch (e)
			{
				console.log("No Audio API support.");
			}
		},
		
		_loadFromFile:function(file)
		{
			var self = this
			var fr = new FileReader();
			
			fr.onload = function(e)
			{
				var fileResult = fr.result;
				self._decode(fileResult);
			}
			
			fr.onerror = function(e)
			{
				console.log("Error with loading from file.");
			}
			
			fr.readAsArrayBuffer(file);
		},
		
		_loadFromRequest:function()
		{
			var self = this;
			var request = new XMLHttpRequest();

			request.open('GET', this.file, true);
			request.responseType = 'arraybuffer';
			
			request.onload = function()
			{
				var audioData = request.response;
				self._decode(audioData);
			}
			
			request.send();
		},
		
		_load:function()
		{
			this._loadFromRequest();
		},
		
		_decode:function(audioData)
		{
			var self = this;
			this.audioContext.decodeAudioData(audioData,
				function(buffer)
				{
					self.buffer = buffer;
					if(self.bufferLoaded)
						self.bufferLoaded();
				},
				function(e)
				{
					console.log("Error with decoding audio data.");
				}
			);
		},
		
		_disconnect:function()
		{
			for(var i = 0;i < this.analyser.length;i++)
			{
				this.analyser[i].disconnect();
				this.processor[i].disconnect();
			}
			
			this.splitter.disconnect();
			this.gainNode.disconnect();
			this.source.disconnect();
		},
		
		_prepare:function()
		{
			if(this.buffer === null) throw "No buffer to prepare.";
			
			this.source 		= this.audioContext.createBufferSource();
			this.source.buffer 	= this.buffer;
			
			this.source.loop = this.loop;
			
			if(!this.source.start)
			{
				this.source.start = this.source.noteOn
				this.source.stop = this.source.noteOff
			}
			
			this.gainNode = this.audioContext.createGain();
			this.processor[0] = this.audioContext.createScriptProcessor(2048, 1, 1);
			this.processor[1] = this.audioContext.createScriptProcessor(2048, 1, 1);
			
			this.splitter = this.audioContext.createChannelSplitter();
			this.analyser[0] = this.audioContext.createAnalyser();
			this.analyser[1] = this.audioContext.createAnalyser();
			
			this.source.connect(this.gainNode);
			this.gainNode.connect(this.splitter);
			
			for(var i = 0;i < this.analyser.length;i++)
			{
				this.analyser[i].smoothingTimeConstant = 0.3;
				this.analyser[i].fftSize = 1024;
				this.splitter.connect(this.analyser[i],i,0);
				this.analyser[i].connect(this.processor[i]);
				this.processor[i].connect(this.audioContext.destination);
			}
			
			this.gainNode.connect(this.audioContext.destination);
			
			var self = this;
			this.source.onended = function(e)
			{
				self.end();
			}
			
			for(var i = 0;i < this.processor.length;i++)
			{
				this.processor[i].onaudioprocess = function(e)
				{
					var index = self.processor.indexOf(this);
					if(index == 0)
					{
						self.gainNode.gain.value = self.volume;
					}
					
					self.frequencyBinCount[index] = self.analyser[index].frequencyBinCount;
					
					self.frequencyArray[index] = new Uint8Array(self.frequencyBinCount[index]);
					self.analyser[index].getByteFrequencyData(self.frequencyArray[index]);
					
					self.timeDomainArray[index] = new Uint8Array(self.frequencyBinCount[index]);
					self.analyser[index].getByteTimeDomainData(self.timeDomainArray[index]);
				}
			}
		},
		
		getRemainingTime:function()
		{
			if(this.buffer === null) return 0;
			return parseInt(this.buffer.duration - this.audioContext.currentTime, 10);
		},
		
		getRemainingMin:function()
		{
			return Math.floor(this.getRemainingTime()/60,10);
		},
		
		getRemainingSec:function()
		{
			return this.getRemainingTime() - this.getRemainingMin()*60;
		},
		
		printTimeLeft:function()
		{
			return '-' + this.getRemainingMin() + ':' + 
				(this.getRemainingSec() > 9 ? this.getRemainingSec() : '0' + this.getRemainingSec());
		},
		
		end:function(time)
		{
			if(this.playing && this.source != null)
			{
				var time 			= time 			|| 0;
				
				this.startOffset += this.audioContext.currentTime - this.startTime;
				this.source.stop(time);
				this.playing = false;
				
				this.frequencyBinCount 	= [];
				
				this.frequencyArray 	= [];
				this.timeDomainArray 	= [];
			
				this._disconnect();
			}
		},
		
		stop:function(time)
		{
			return this.end(time);
		},
		
		play:function(time,bufferOffset)
		{
			var time 			= time 			|| 0;
			var bufferOffset 	= bufferOffset 	|| 0;
			try
			{
				this.end();
				this._prepare();
				this.startTime = this.audioContext.currentTime;
				this.source.start(time,bufferOffset);
				this.playing = true;
			}
			catch(e){};
		},
		
		resume:function()
		{
			this.play(0,this.startOffset % this.buffer.duration)
		},
		
		setVolume:function(factor)
		{
			this.volume = factor;
			return this;
		},
		
		getAverageVolume:function(channel)
		{
			var average = 0;
			if(this.playing && this.frequencyArray.length > 0 && this.frequencyArray[channel])
			{
				var values = 0;
				var length = this.frequencyArray[channel].length;
				for(var i = 0; i < length; i++)
				{
					values += this.frequencyArray[channel][i];
				}
				
				average = values / length;
			}
			
			return average;
		}
	};

	function PHAudioSpectrum(audio,options)
	{
		var options 		= options || {};
		options.width 		= options.width 		|| 300;
		options.height 		= options.height 		|| 255;
		options.color 		= options.color 		|| [255,255,255,1.0];
		options.meterWidth 	= options.meterWidth 	|| 8;
		options.capHeight 	= options.capHeight 	|| 2;
		options.gapWidth 	= options.gapWidth 		|| 2;
		
		options.colors 		= options.colors 		|| [
		
			[0,255,0,1.0],
			[255,255,0,1.0],
			[255,0,0,1.0]
		
		];
		
		PHObject.call(this,options);
		
		this.audio = audio;
		this.channel = 0;
		
		this.colors = options.colors;
		
		this.meterWidth = options.meterWidth;
		this.capHeight 	= options.capHeight;
		this.gapWidth 	= options.gapWidth;
		
		this.width = options.width;
		this.height = options.height;
		
		this.step = 0;
		this.capYPositionArray 	= [];
		
		return this;
	}

	PHAudioSpectrum.prototype = Object.create(PHObject.prototype);
	PHAudioSpectrum.prototype.constructor = PHAudioSpectrum;

	PHAudioSpectrum.prototype.animate = function(canvas,index){

		if(!this.audio) return this;

		if(this.audio.playing && this.audio.frequencyArray.length != 0)
		{
			this.step = Math.round(this.audio.frequencyArray[this.channel].length / (this.width/(this.meterWidth + this.gapWidth)));
		}
		else
		{
			this.capYPositionArray = [];
			this.step = 0;
		}
		
		return this;
	}

	PHAudioSpectrum.prototype.draw = function(canvas,index){

		if(!this.audio) return this;

		canvas.context.save();
		
		var gradient = canvas.context.createLinearGradient(0, this.pos[1], 0, this.pos[1]+this.height);
		gradient.addColorStop(0.33, 'rgba(' + this.colors[0].join() + ')');
		gradient.addColorStop(0.66, 'rgba(' + this.colors[1].join() + ')');
		gradient.addColorStop(1.0, 	'rgba(' + this.colors[2].join() + ')');
		
		for (var i = 0; i < (this.width/(this.meterWidth + this.gapWidth)); i++)
		{
			var value = 0;
			if(this.audio.frequencyArray.length != 0)
			{
				value = this.audio.frequencyArray[this.channel][i * this.step]*this.height/255;
				value = Math.ceil(value);
			}
			
			if(this.capYPositionArray.length < Math.round((this.width/(this.meterWidth + this.gapWidth))))
			{
				this.capYPositionArray.push(value);
			}
			
			canvas.context.fillStyle = 'rgba(' + this.color.join() + ')';
			
			var x = this.pos[0] + (this.meterWidth + this.gapWidth)*i;
			if (value < this.capYPositionArray[i])
			{
				canvas.context.fillRect(x, this.pos[1] + (--this.capYPositionArray[i]), this.meterWidth, this.capHeight);
			}
			else
			{
				canvas.context.fillRect(x, this.pos[1] + value, this.meterWidth, this.capHeight);
				this.capYPositionArray[i] = value;
			}
			
			canvas.context.fillStyle = gradient;
			canvas.context.fillRect(x, this.pos[1], this.meterWidth,(value + this.capHeight));
		}
		
		canvas.context.restore();
		
		return this;
	};

	PHInject(PHAudio,"PHAudio");
	PHInject(PHAudioSpectrum,"PHAudioSpectrum");
	
})();


