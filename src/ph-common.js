
(function(){

	function PHStroke(options)
	{
		options.stroke 		= options.stroke 			|| 0;
		options.strokeColor 	= options.strokeColor 	|| [255,255,255,1];

		this.stroke = options.stroke;
		this.strokeColor = options.strokeColor;
		
		this.setStroke = function(stroke){this.stroke = stroke; return this;}
		this.setStrokeColor = function(color){this.strokeColor = color; return this;}
	}

	function PHText(options)
	{
		var options 		= options 			|| {};
		options.msg 		= options.msg 		|| "";
		options.font 		= options.font 		|| "Consolas";
		options.size 		= options.size 		|| 12;
		options.align 		= options.align 	|| "left";
		options.baseline 	= options.baseline 	|| "alphabetic";
		
		PHObject.call(this,options);
		PHStroke.call(this,options);
		
		this.msg 		= options.msg;
		this.fontSize 	= options.size;
		this.fontFamily = options.font;
		this.align 		= options.align;
		this.baseline 	= options.baseline;
		this.updateFont();
		
		return this;
	}

	PHText.prototype = Object.create(PHObject.prototype);
	PHText.prototype.constructor = PHText;

	PHText.prototype.setMsg = function(msg){this.msg = msg; return this;};
	PHText.prototype.setFont = function(font){this.fontFamily = font; this.updateFont(); return this;};
	PHText.prototype.setSize = function(size){this.fontSize = size; this.updateFont(); return this;};
	PHText.prototype.setAlign = function(align){this.align = align; return this;};
	PHText.prototype.setBaseline = function(baseline){this.baseline = baseline; return this;};

	PHText.prototype.updateFont = function(){this.font = this.fontSize + "px " + this.fontFamily; return this;};

	PHText.prototype.draw = function(canvas,index){

		this.__fadeIn(canvas);
		this.__fadeOut(canvas);

		canvas.context.save();
		
		canvas.context.translate(this.intPos[0],this.intPos[1]);
		canvas.context.rotate(PHUtility.degToRad(this.rotate));
		
		canvas.context.font = this.font;
		canvas.context.textAlign = this.align;
		canvas.context.textBaseline = this.baseline;
		
		if(this.stroke > 0)
		{
			canvas.context.strokeStyle = 'rgba(' + this.strokeColor.join() + ')';
			canvas.context.lineWidth = this.stroke;
			canvas.context.strokeText(this.msg,0,0);
		}
		
		canvas.context.fillStyle = 'rgba(' + this.color.join() + ')';
		canvas.context.fillText(this.msg,0,0);
		
		canvas.context.restore();
		
		return this;
	};

	PHText.prototype.textWidth = function()
	{
		var canvas = document.createElement("canvas");
		var context = canvas.getContext('2d');
		
		context.font = this.font;
		return context.measureText(this.msg).width;
	};

	function PHRect(options)
	{
		var options = options || {};
		
		PHObject.call(this,options);
		PHStroke.call(this,options);
		
		this.width = options.width;
		this.height = options.height;
		
		return this;
	}

	PHRect.prototype = Object.create(PHObject.prototype);
	PHRect.prototype.constructor = PHRect;

	PHRect.prototype.setWidth = function(width){this.width = width; return this;};
	PHRect.prototype.setHeight = function(height){this.height = height; return this;};

	PHRect.prototype.draw = function(canvas,index)
	{
		this.__fadeIn(canvas);
		this.__fadeOut(canvas);

		canvas.context.save();
		
		canvas.context.translate(this.intPos[0],this.intPos[1]);
		canvas.context.rotate(PHUtility.degToRad(this.rotate));
		
		if(this.stroke > 0)
		{
			canvas.context.strokeStyle = 'rgba(' + this.strokeColor.join() + ')';
			canvas.context.lineWidth = this.stroke;
			
			canvas.context.beginPath();
			canvas.context.strokeRect(0,0,this.width*canvas.getSizeRatio(),this.height*canvas.getSizeRatio());
			canvas.context.closePath();
		}
		
		canvas.context.beginPath();
		canvas.context.rect(0,0,this.width*canvas.getSizeRatio(),this.height*canvas.getSizeRatio());
		canvas.context.closePath();
		
		canvas.context.fillStyle = 'rgba(' + this.color.join() + ')';
		canvas.context.fill();
		
		canvas.context.restore();
		
		return this;
	};

	function PHArc(options)
	{
		var options = options || {};
		options.radius = options.radius || 5;
		options.startAngle = options.startAngle || 0;
		options.endAngle = options.endAngle || 2*Math.PI;
		
		PHObject.call(this,options);
		PHStroke.call(this,options);
		
		this.radius = options.radius;
		this.startAngle = options.startAngle;
		this.endAngle = options.endAngle;
		this.counterClockwise = true;
		
	}


	PHArc.prototype = Object.create(PHObject.prototype);
	PHArc.prototype.constructor = PHArc;

	PHArc.prototype.getRadius = function(){return this.radius;};
	PHArc.prototype.setRadius = function(radius){this.radius = radius; return this;};

	PHArc.prototype.draw = function(canvas,index)
	{
		this.__fadeIn(canvas);
		this.__fadeOut(canvas);

		canvas.context.save();
		
		canvas.context.translate(this.intPos[0],this.intPos[1]);
		canvas.context.rotate(PHUtility.degToRad(this.rotate));

		canvas.context.beginPath();
		canvas.context.arc(0,0, Math.ceil(this.radius*canvas.getSizeRatio()), this.startAngle, this.endAngle, this.counterClockwise);
		canvas.context.closePath();
		
		if(this.stroke > 0)
		{
			canvas.context.strokeStyle = 'rgba(' + this.strokeColor.join() + ')';
			canvas.context.lineWidth = this.stroke;
			canvas.context.stroke()
		}
		
		canvas.context.fillStyle = 'rgba(' + this.color.join() + ')';
		canvas.context.fill();
		
		canvas.context.restore();
		
		return this;
	}

	function PHImg(options)
	{
		var options = options || {};
		options.cropWidth 		= options.cropWidth 	|| [0,0];
		options.cropHeight 		= options.cropHeight 	|| [0,0];
		options.spritemap 		= options.spritemap 	|| [0,0];
		options.flip 			= options.flip 			|| false;
		options.renderWidth 	= options.renderWidth 	|| null;
		options.renderHeight 	= options.renderHeight 	|| null;

		PHObject.call(this,options);

		this.imgData 		= {};
		this.img 			= options.img;
		this.spritemap 	= options.spritemap;
		this.flip 				= options.flip;
		
		this.width 	= options.width;
		this.height = options.height;

		this.cropWidth 	= options.cropWidth;
		this.cropHeight 	= options.cropHeight;
		
		this.renderWidth 	= options.renderWidth;
		this.renderHeight 	= options.renderHeight;
		
		this.updateSize();
		
		return this;
	}

	PHImg.prototype = Object.create(PHObject.prototype);
	PHImg.prototype.constructor = PHImg;

	PHImg.prototype.getSize = function()
	{
		var left 	= this.cropWidth[0];
		var right 	= this.cropWidth[1];
		
		var top 	= this.cropHeight[0];
		var bottom 	= this.cropHeight[1];

		var cropWidth 	= Math.floor(this.width*this.spritemap[0] + left);
		var totalWidth 	= this.width - (left + right);
		
		var cropHeight 	= Math.floor(this.height*this.spritemap[1] + top);
		var totalHeight = this.height - (top + bottom);
		
		return {cropWidth:cropWidth,totalWidth:totalWidth,cropHeight:cropHeight,totalHeight:totalHeight};
	};

	PHImg.prototype.updateSize = function()
	{
		var size = this.getSize();
		if(this.renderWidth == null)
			this.renderWidth = size.totalWidth;
			
		if(this.renderHeight == null)
			this.renderHeight = size.totalHeight;
	};

	PHImg.prototype.getCenter = function()
	{
		var size = this.getSize();

		var x = Math.round(this.pos[0] + size.totalWidth/2);
		var y = Math.round(this.pos[1] + size.totalHeight/2);
		
		return [x,y];
	};

	PHImg.prototype.draw = function(canvas,index)
	{
		this.__fadeIn(canvas);
		this.__fadeOut(canvas);

		canvas.context.save();
		canvas.context.translate(this.intPos[0],this.intPos[1]);
		canvas.context.rotate(PHUtility.degToRad(this.rotate));
		canvas.context.globalAlpha = this.color[3];
		
		var size = this.getSize();
		
		var totalWidth = Math.ceil(this.renderWidth*canvas.getSizeRatio());
		var totalHeight = Math.ceil(this.renderHeight*canvas.getSizeRatio());
		
		if(this.flip)
		{
			context.translate(totalWidth,0);
			context.scale(-1,1);
		} 
		
		canvas.context.drawImage(this.img,
			size.cropWidth,size.cropHeight, 
			size.totalWidth,size.totalHeight, 
			0,0,
			Math.ceil(totalWidth), Math.ceil(totalHeight)
		);
			
		canvas.context.restore();
		
		return this;
	};

	function PHImgAnim(options)
	{
		var options = options || {};
		options.frameTime 	= options.frameTime || 1;
		
		if(typeof options.remove == 'undefined')
			options.remove = 1;
		
		PHImg.call(this,options);

		this.frames			= options.frames;
		this.frameTime 		= options.frameTime;
		this.remove 		= options.remove;
		
		this.nextFrame 		= 0;
		this.frameIndex 	= 0;
	}

	PHImgAnim.prototype = Object.create(PHImg.prototype);
	PHImgAnim.prototype.constructor = PHImgAnim;

	PHImgAnim.prototype.animate = function(canvas,index)
	{
		if(this.frames < 2) return;

		if(this.nextFrame < 1)
		{
			this.nextFrame = canvas.getNow() + this.frameTime;
		}

		if(this.nextFrame < canvas.getFNow())
		{
			if(this.frameIndex > this.frames)
			{
				if(this.remove)
				{
					canvas.remove(this.id,index);
				}
				else
				{
					this.frameIndex = 0;
					this.spritemap[0] = 0;
					this.nextFrame = canvas.getNow() + this.frameTime;
				}
				
				return;
			}
			else
			{
				this.frameIndex += 1;
				this.nextFrame = canvas.getNow() + this.frameTime;
			}
		}
		
		this.spritemap[0] = this.frameIndex;
	};
	
	PHInject(PHStroke,"PHStroke");
	PHInject(PHText,"PHText");
	PHInject(PHRect,"PHRect");
	PHInject(PHArc,"PHArc");
	PHInject(PHImg,"PHImg");
	PHInject(PHImgAnim,"PHImgAnim");
	
})();

