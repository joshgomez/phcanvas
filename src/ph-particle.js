
(function(){

	function PHParticleOptions(options)
	{
		var options 			= options 					|| {};
		options.life 			= options.life 				|| PHUtility.randNum(2000,3000);
		options.radius 			= options.radius 			|| PHUtility.randNum(15,30);
		options.decay 			= options.decay 			|| PHUtility.randNum(2,10);
		options.speed 			= options.speed 			|| PHUtility.randNum(-50,-100);
		options.acceleration 	= options.acceleration 		|| PHUtility.randNum(-20,20);
		options.angle 			= options.angle 			|| PHUtility.randNum(60,120);
		options.reverse 		= options.reverse 			|| 0;
		
		if(!options.color)
		{
			options.color 		= [];
			options.color[0] 	= 255;
			options.color[1]  	= PHUtility.randNum(50,255);
			options.color[2]  	= PHUtility.randNum(1,1);
			options.color[3]   	= 1.0;
		}
		
		return options;
	}

	function PHParticleBase(options)
	{
		this.life 		= options.life;
		this.decay 		= options.decay;
		this.reverse 	= options.reverse;
	}

	function PHParticle(options)
	{
		var options = options || {};
		options	= PHParticleOptions(options);
		
		PHArc.call(this,options);
		PHMovement.call(this,options);
		PHParticleBase.call(this,options);
	}

	PHParticle.prototype = Object.create(PHArc.prototype);
	PHParticle.prototype.constructor = PHParticle;

	PHParticle.prototype.animate = function(canvas,index)
	{
		var size = this.decay*canvas.getSizeRatio()*canvas.getDelta();
		if(this.reverse > 0) this.radius += size;
		else this.radius -= size;
		
		var acceleration = this.getVelocity(canvas);
		this.setSpeed(acceleration);
		
		var direction = PHUtility.degToCoord(this.angle);
		var newPos = this.getMovePos(direction,canvas);
		this.setPos(newPos[0],newPos[1]);
		
		if(this.life > 0)
			this.color[3] = (Math.abs(canvas.getNow()-this.created-this.life)/this.life).toFixed(2);

		if((this.life > 0 && this.life + this.created < canvas.getNow()) || (!this.reverse && this.radius < 1) || (this.reverse && this.radius > this.reverse))
		{
			canvas.remove(this.id,index);
		}
	};

	PHParticle.prototype.draw = function(canvas)
	{
		canvas.context.save();

		canvas.context.globalCompositeOperation = "lighter";

		canvas.context.beginPath();

		var gradient = canvas.context.createRadialGradient(this.pos[0], this.pos[1], 0, this.pos[0], this.pos[1], this.radius);
		
		gradient.addColorStop(0, "rgba("+this.color[0]+", "+this.color[1]+", "+this.color[2]+", "+this.color[3]+")");
		gradient.addColorStop(0.5, "rgba("+this.color[0]+", "+this.color[1]+", "+this.color[2]+", "+this.color[3]+")");
		gradient.addColorStop(1, "rgba("+this.color[0]+", "+this.color[1]+", "+this.color[2]+", 0)");

		var radius = Math.ceil(this.radius*canvas.getSizeRatio());
		
		canvas.context.fillStyle = gradient;
		canvas.context.arc(this.intPos[0], this.intPos[1], radius, Math.PI*2, false);
		canvas.context.fill();
		
		canvas.context.closePath();
		
		canvas.context.restore();
	};

	function PHParticleImg(options)
	{
		var options = options || {};
		options 		= PHParticleOptions(options);
		options.remove 	= 0;
		
		PHImgAnim.call(this,options);
		PHMovement.call(this,options);
		PHParticleBase.call(this,options);
	}

	PHParticleImg.prototype = Object.create(PHImgAnim.prototype);
	PHParticleImg.prototype.constructor = PHParticleImg;

	PHParticleImg.prototype.frameAnimate = PHParticleImg.prototype.animate;
	PHParticleImg.prototype.animate = function(canvas,index)
	{
		this.frameAnimate(canvas,index);
		
		var size = this.decay*canvas.getSizeRatio()*canvas.getDelta();
		
		if(this.reverse > 0)
		{
			this.renderWidth 	+= size;
			this.renderHeight 	+= size;
			
			var alpha = ((this.reverse-this.renderHeight)/this.reverse);
			if(alpha < 0) alpha = 0;
			
			this.setAlpha(alpha);
		}
		else
		{
			this.renderWidth 	-= size;
			this.renderHeight 	-= size;
		}

		var acceleration = this.getVelocity(canvas);
		this.setSpeed(acceleration);
		
		var direction = PHUtility.degToCoord(this.angle);
		var newPos = this.getMovePos(direction,canvas);
		this.setPos(newPos[0],newPos[1]);
		
		if((this.life > 0 && this.life + this.created < canvas.getNow()) || 
		(!this.reverse && (this.renderWidth < 1 || this.renderHeigh < 1)) || 
		(this.reverse && (this.renderWidth > this.reverse || this.renderHeigh > this.reverse)))
		{
			canvas.remove(this.id,index);
		}
	};

	PHInject(PHParticleOptions,"PHParticleOptions");
	PHInject(PHParticle,"PHParticle");
	PHInject(PHParticleImg,"PHParticleImg");
	
})();

