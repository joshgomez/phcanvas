﻿
(function(){

	function PHSpark(options)
	{
		var options 			= options 						|| {};
		options.speed 			= options.speed 				|| PHUtility.randNum(300,500);
		options.acceleration 	= options.acceleration 			|| PHUtility.randNum(50,100);
		options.gravity 		= options.gravity 				|| 1;
		options.angle 			= options.angle 				|| PHUtility.randNum(0,360);
		options.color 			= options.color 				|| [120,100,PHUtility.randNum(50,80),1.0];
		options.decay 			= options.decay 				|| 1.4;
		options.radius 			= options.radius 				|| 2;
		options.coordinateCount	= options.coordinateCount 		|| 10;

		PHObject.call(this,options);
		PHMovement.call(this,options);
		
		this.coordinates = [];
		this.coordinateCount = options.coordinateCount;
		
		while(this.coordinateCount--)
		{
			this.coordinates.push([this.intPos[0],this.intPos[1]]);
		}

		this.alpha = 1.0;
		this.decay = options.decay;
		
		var color = PHUtility.hslToRGB(this.color[0],this.color[1],this.color[2]);
		this.head = new PHArc({
			pos:this.pos,
			color:[color.r,color.g,color.b,this.color[3]],
			radius:options.radius
		});
	}

	PHSpark.prototype = Object.create(PHObject.prototype);
	PHSpark.prototype.constructor = PHSpark;

	PHSpark.prototype.animate = function(canvas,index)
	{
		this.coordinates.pop();
		this.coordinates.unshift([this.intPos[0],this.intPos[1]]);
		
		var acceleration = this.getVelocity(canvas);
		this.setSpeed(acceleration);

		var direction = PHUtility.degToCoord(this.angle);
		var newPos = this.getMovePos(direction,canvas);
		
		this.setPos(newPos);

		var decay = this.decay*canvas.getDelta();

		if(this.head)
		{
			this.head.setPos(newPos);
			this.head.setAlpha(this.alpha);
		}

		this.alpha -= decay;
		this.setAlpha(parseFloat(this.alpha.toFixed(2)));

		if(this.color[3] <= decay)
		{
			canvas.remove(this.id,index);
		}
	};

	PHSpark.prototype.draw = function(canvas,index)
	{
		if(this.head)
		{
			this.head.draw(canvas,index);
		}
		
		canvas.context.save();
		
		canvas.context.globalCompositeOperation = 'lighter';
		
		canvas.context.beginPath();
		
		canvas.context.moveTo( this.coordinates[ this.coordinates.length - 1 ][ 0 ], this.coordinates[ this.coordinates.length - 1 ][ 1 ] );
		canvas.context.lineTo( this.intPos[0], this.intPos[1] );
		canvas.context.lineWidth = 1;
		canvas.context.strokeStyle = 'hsla(' + this.color[0] + ', ' + this.color[1] + '%, ' + this.color[2] + '%, ' + this.color[3] + ')';
		canvas.context.stroke();
		
		canvas.context.closePath();
		
		canvas.context.restore();
	}

	PHInject(PHSpark,"PHSpark");
	
})();

